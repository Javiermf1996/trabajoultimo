<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use \PDF;

class MainController extends Controller
{
    function index()
    {
     return view('login');
    }

    function pdf(){
       
        $users = \App\User::all();

        $pdf = PDF::loadview('emails.sendmail', ['users' => $users]);
        return $pdf->download('contenido.pdf');
    }

    function checklogin(Request $request)
    {
     $this->validate($request, [
      'email'   => 'required|email',
      'password'  => 'required|alphaNum|min:3',
     ]);
     if($request->input('g-recaptcha-response') != ''){

     

     $user_data = array(
      'email'  => $request->get('email'),
      'password' => $request->get('password'),
     );

     if(Auth::attempt($user_data))
     {
      return redirect('main/successlogin');
     }
     else
     {
      return back()->with('error', 'Wrong Login Details');
     }

    }else{
        return back()->with('error', 'Rellene la captcha');
    }
    }

    function successlogin()
    {
     return view('successlogin');
    }

    function logout()
    {
     Auth::logout();
     return redirect('main');
    }
}

?>