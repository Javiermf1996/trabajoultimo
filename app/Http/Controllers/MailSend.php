<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Mail\SendMail;
use \PDF;
class MailSend extends Controller
{
    public function mailsend()
    {
        $users = \App\User::all();

        $pdf = PDF::loadview('emails.sendmail', ['users' => $users]);

        \Mail::send('emails.sendmail',['users' => $users],function ($mail) use ($pdf) {
            $mail->to('from@example.com');
            $mail->attachData($pdf->output(), 'test.pdf');
        });
        return view('emails.thanks');
 
    }
}